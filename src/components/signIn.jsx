import React, { Component } from "react";
import axios from "axios";
import { BrowserRouter as Router } from "react-router-dom";
import auth from "./auth";
import "./signIn.css";
import { MDBInput } from "mdbreact";
import {
  MDBBtn,
  MDBCard,
  MDBCardHeader,
  MDBCardBody,
  MDBIcon,
  MDBModal,
  MDBContainer,
  MDBModalBody,
  MDBModalFooter,
  MDBRow,
  MDBCol,
  MDBLink,
  MDBTypography,
} from "mdbreact";

class SignIn extends Component {
  state = {
    modal: false,
    otpModal: false,
    registerModal: false,
  };
  toggle = () => {
    this.setState({
      modal: !this.state.modal,
    });
  };
  otpToggle = () => {
    this.setState({
      otpModal: !this.state.otpModal,
    });
  };

  toggleRegister = () => {
    this.setState({
      registerModal: !this.state.registerModal,
    });
  };
  constructor(props) {
    super(props);
    let loggedIn = false;
    this.state = {
      otp: "",
      users: [],
      emailId: "",
      password: "",
      emailIdError: "",
      usernameError: "",
      passwordError: "",
      forgotEmailId: "",
      loggedIn,
    };
  }

  componentDidMount() {
    this.getRoles();
    this.getUsers();
  }

  reset() {
    this.setState({
      otp: "",
      username: "",
      password: "",
      emailId: "",
      usernameError: "",
      emailIdError: "",
      passwordError: "",
      forgotEmailId: "",
      forgotEmailIdError: "",
    });
  }

  resetError() {
    this.setState({
      usernameError: "",
      emailIdError: "",
      passwordError: "",
      forgotEmailIdError: "",
    });
  }

  handleOtpChange = (event) => {
    this.setState({
      otp: event.target.value,
    });
  };

  handleEmailIdChange = (event) => {
    this.setState({
      emailId: event.target.value,
    });
  };

  handlePasswordChange = (event) => {
    this.setState({
      password: event.target.value,
    });
  };
  handleForgotEmailIdChange = (event) => {
    this.setState({
      forgotEmailId: event.target.value,
    });
  };
  validate = () => {
    this.resetError();
    const passwordRegex = /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,16})/;
    const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if (!this.state.emailId && !this.state.password) {
      this.setState({
        emailIdError: "Enter Your Email-Id",
        passwordError: "Enter Your Password",
      });
      return false;
    } else if (!this.state.emailId) {
      this.setState({ emailIdError: "Email-Id is Required" });
      return false;
    } else if (!emailRegex.test(this.state.emailId)) {
      this.setState({
        emailIdError: "Invalid Email-ID",
      });
      return false;
    }

    if (!this.state.password) {
      this.setState({ passwordError: "Password is Required" });
      return false;
    } else if (this.state.password.length < 4) {
      this.setState({ passwordError: "Password must be 4 characters long." });
      return false;
    } // else if (!passwordRegex.test(this.state.password)) {
    //   this.setState({
    //     passwordError:
    //       "Password must contain at least one Uppercase letter, one lowercase letter, one special character and one number with 8 to 16 character length",
    //   });
    //   return false;
    // }

    return true;
  };

  validateForgotPassword = () => {
    const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    const items = this.state.users;
    let found = items.find((item) => {
      return item.userEmail === this.state.forgotEmailId;
    });
    if (!this.state.forgotEmailId) {
      this.setState({ forgotEmailIdError: "Email-Id is Required" });
      return false;
    } else if (!emailRegex.test(this.state.forgotEmailId)) {
      this.setState({
        forgotEmailIdError: "Invalid Email-ID",
      });
      return false;
    } else if (!found) {
      this.setState({
        forgotEmailIdError: "Email has not registered",
      });
      return false;
    }
    return true;
  };

  handleSubmit = (event) => {
    this.resetError();
    event.preventDefault();
    const isValid = this.validate();
    //const isValid = true;
    if (isValid) {
      this.login();
      // clear form
      this.reset();
    }
  };

  async login() {
    const userEmail = this.state.emailId;
    const password = this.state.password;
    const url = "/public/users/login/" + userEmail + "/" + password;
    console.log(url);
    axios
      .get(url)
      .then((response) => {
        if (response.data.responseCode == "200") {
          const token = {
            uuid: response.data.uuid,
            userId: response.data.user.id,
            username: response.data.user.account.username,
            active: response.data.user.account.active,
            role: response.data.user.authorities[0].authority,
            password: password,
          };
          localStorage.setItem("token", JSON.stringify(token));
          this.setState({ loggedIn: true });
          auth.login(() => {
            this.props.history.push("/home");
          });
        } else if (response.data.responseCode == "500") {
          let failureMsg = response.data.responseMsg;
          alert(`${failureMsg}`);
        }
      })
      .catch((error) => {
        alert(`Invalid login and/or password`);
        console.log(error);
      });
  }

  handleForgetPassword = (event) => {
    event.preventDefault();
    const isValid = this.validateForgotPassword();
    //const isValid = true;
    if (isValid) {
      const emailId = this.state.forgotEmailId;
      const url = "/forgetPassword/" + emailId;
      this.forgetPassword(url);
      this.reset();
      this.toggle();
      this.otpToggle();
    }
  };

  async forgetPassword(url) {
    axios
      .get(url)
      .then((response) => {})
      .catch((error) => {
        console.log(error);
      });
  }

  handleOtpSubmit = (event) => {
    event.preventDefault();
    const isValid = true;
    if (isValid) {
      const otp = this.state.otp;
      const url = "/public/users/otp/" + otp;
      console.log(url);
      this.loginByOtp(url);
      this.reset();
      this.otpToggle();
    }
  };

  async loginByOtp(url) {
    console.log(url);
    axios
      .get(url)
      .then((response) => {
        if (response.data.responseCode == "200") {
          const token = {
            uuid: response.data.uuid,
            userId: response.data.user.id,
            username: response.data.user.account.username,
            active: response.data.user.account.active,
            role: response.data.user.authorities[0].authority,
            password: "",
          };
          localStorage.setItem("token", JSON.stringify(token));
          this.setState({ loggedIn: true });
          auth.login(() => {
            this.props.history.push("/home");
          });
        } else if (response.data.responseCode == "500") {
          let failureMsg = response.data.responseMsg;
          alert(`${failureMsg}`);
        }
      })
      .catch((error) => {
        console.log(error);
        alert(`Invalid OTP`);
      });
  }

  async getRoles() {
    axios
      .get("/dalrada/user/roleResource/roles")
      .then((response) => {
        const roles = response.data
          .filter((item) => item.respBody.status === 1)
          .map((resp) => {
            return {
              value: resp.respBody.roleName,
              label: resp.respBody.roleName,
            };
          });

        this.setState({
          roleItems: roles,
        });
      })
      .catch((error) => console.log(error));
  }

  async getUsers() {
    const url = "dalrada/user/userResource/users";
    axios
      .get(url)
      .then((response) => {
        let userList = response.data.map((item) => {
          return item.respBody;
        });

        this.setState({
          users: userList,
        });
      })
      .catch((error) => console.log(error));
  }

  render() {
    return (
      <Router>
        <div className="bg sticky">
          <MDBRow className="mt-3 ">
            <MDBCol middle={true}>
              <div className="brandname mb-3">
                <div className="dalrada text-center mb-n2">Dalrada</div>
                <div className="financial text-center mt-n4">Financial</div>
              </div>
            </MDBCol>
          </MDBRow>
          <div className="row">
            <div className="col-xl-6 col-sm-8 offset-xl-3 offset-sm-1 ">
              <MDBCard>
                <MDBCardHeader
                  titleClass="d-inline title"
                  color="brown lighten-5"
                  className="text-center darken-3 white-text"
                >
                  Sign In
                </MDBCardHeader>
                <MDBCardBody>
                  <form onSubmit={this.handleSubmit}>
                  <div className="form-group">
                    <MDBInput label="Email Id:" 
                              value={this.state.emailId}
                              onChange={this.handleEmailIdChange}
                              type="text"
                              className="form-control text-center"
                              iconClass=" grey lighten-3"
                              id="formGroupExampleInput" />
                        <div className="text-center red-text">
                            {this.state.emailIdError}
                        </div>      
                    <MDBInput label="Password:"
                              value={this.state.password}
                              onChange={this.handlePasswordChange}
                              type="password"
                              iconClass=" grey lighten-3"
                              className="form-control text-center" />
                        <div className="text-center red-text">
                           {this.state.passwordError}
                        </div>       
                  </div>
                    

                    <div className="text-center mt-4">
                      <MDBBtn
                        color="primary"
                        type="submit"
                        className="mb-2 rounded-pill"
                        size="sm"
                      >
                        Sign In
                        <MDBIcon icon="paper-plane" className="ml-1" />
                      </MDBBtn>
                    </div>
                    <div className="row">
                      <div className="col-6 offset-4">
                        <MDBLink
                          color="success"
                          className="ml-2"
                          onClick={this.toggle}
                        >
                          Forgot Password?
                        </MDBLink>
                      </div>
                    </div>
                  </form>
                </MDBCardBody>
              </MDBCard>
            </div>

            <MDBModal isOpen={this.state.modal} toggle={this.toggle}>
              <MDBModalBody>
                <MDBCard>
                  <MDBCardHeader
                    titleClass="d-inline title"
                    color="brown lighten-5"
                    type="text"
                    className="text-center  darken-3 white-text"
                  >
                    Forgot Password
                    <button
                      variant="contained"
                      style={{ float: "right" }}
                      color="brown lighten-5"
                      className="float-right"
                      onClick={this.toggle}
                    >
                      <MDBIcon icon="times" size="lg" className="red-text " />
                    </button>
                  </MDBCardHeader>
                  <MDBCardBody>
                    <form onSubmit={this.handleForgetPassword.bind(this)}>
                      <MDBContainer>
                      <MDBInput label="Your e-mail" 
                               value={this.state.forgotEmailId}
                               onChange={this.handleForgotEmailIdChange}
                               type="text"
                               className="form-control text-center"
                               iconClass="dark-grey"
                               id="formGroupExampleInput"  />
                               <div className="text-center red-text">
                                 {this.state.forgotEmailIdError}
                              </div>
                       
                      </MDBContainer>
                      <div className="text-center mt-1-half">
                        <MDBBtn
                          color="primary"
                          type="submit"
                          className="mb-2 mt-3 rounded-pill"
                          size="sm"
                        >
                          Send Mail
                          <MDBIcon icon="paper-plane" className="ml-1" />
                        </MDBBtn>
                        <MDBBtn
                          color="danger"
                          className="mb-2 mt-3 rounded-pill"
                          size="sm"
                          type="reset"
                          onClick={this.reset.bind(this)}
                        >
                          RESET
                        </MDBBtn>
                      </div>
                    </form>
                  </MDBCardBody>
                </MDBCard>
              </MDBModalBody>
            </MDBModal>
            <MDBModal isOpen={this.state.otpModal} toggle={this.otpToggle}>
              <MDBModalBody>
                <MDBCard>
                  <MDBCardHeader
                    titleClass="d-inline title"
                    color="brown lighten-5"
                    type="text"
                    className="text-center  darken-3 white-text"
                  >
                    Enter OTP
                    <button
                      variant="contained"
                      style={{ float: "right" }}
                      color="brown lighten-5"
                      className="float-right"
                      onClick={this.otpToggle}
                    >
                      <MDBIcon icon="times" size="lg" className="red-text " />
                    </button>
                  </MDBCardHeader>
                  <MDBCardBody>
                    <form onSubmit={this.handleOtpSubmit.bind(this)}>
                      <MDBContainer>
                      <MDBInput label="OTP:" 
                                value={this.state.otp}
                                onChange={this.handleOtpChange}
                                type="text"
                                className="form-control text-center"
                                iconClass="dark-grey"
                                id="formGroupExampleInput"/>
                        <div className="text-center red-text">
                          {this.state.forgotOtpError}
                        </div>        
                        
                      </MDBContainer>
                      <div className="text-center mt-1-half">
                        <MDBBtn
                          color="primary"
                          type="submit"
                          className="mb-2 mt-3 rounded-pill"
                          size="sm"
                        >
                          Log in
                          <MDBIcon icon="paper-plane" className="ml-1" />
                        </MDBBtn>
                        <MDBBtn
                          color="danger"
                          className="mb-2 mt-3 rounded-pill"
                          size="sm"
                          type="reset"
                          onClick={this.reset.bind(this)}
                        >
                          RESET
                        </MDBBtn>
                      </div>
                    </form>
                  </MDBCardBody>
                </MDBCard>
              </MDBModalBody>
            </MDBModal>
          </div>
        </div>
      </Router>
    );
  }
}

export default SignIn;
