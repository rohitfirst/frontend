import React, { Component } from "react";
import "./logo.css";
class Logo extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <div className="row">
        <div className="col-4 offset-5 logo">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
          <div className="dalrada mt-2">Dalrada</div>
          <div className="financial mt-n5 pb-5 mb-5">Financial</div>
        </div>
      </div>
    );
  }
}

export default Logo;
