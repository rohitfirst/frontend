import React, { Component } from "react";
import axios from "axios";
import { Redirect } from "react-router-dom";
import {
  MDBNavbar,
  MDBNavbarBrand,
  MDBNavbarNav,
  MDBNavItem,
  MDBNavLink,
  MDBNavbarToggler,
  MDBCollapse,
  MDBIcon,
  MDBLink,
  MDBBtn,
  MDBBtnGroup,
} from "mdbreact";
import { BrowserRouter as Router } from "react-router-dom";
import { Route, Switch } from "react-router-dom";
import auth from "./auth";
import SignIn from "./signIn";
import User from "./user";
import Upload from "./upload";
import Reconcile from "./reconcile";
import Warehouse from "./warehouse";
import Role from "./role";

import "./home.css";
import Loader from "react-loader-spinner";
import Logo from "./logo";
class Home extends Component {
  state = {
    loggedIn: true,
    isOpen: false,
    modal: false,
    collapseID: "",
    userName: "",
  };

  componentDidMount() {
    let token = JSON.parse(localStorage.getItem("token"));
    if (token) {
      this.setState({
        loggedIn: true,
        userName: token.username,
      });
    } else {
      this.setState({
        loggedIn: false,
        //userName: token.username,
      });
    }
  }
  toggleCollapse = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };

  toggleLinks = (collapseID) => () => {
    this.setState((prevState) => ({
      collapseID: prevState.collapseID !== collapseID ? collapseID : "",
    }));
  };

  async signOut() {
    let token = JSON.parse(localStorage.getItem("token"));
    if (token) {
      const id = token.uuid;
      const url = "/logout/" + id;
      const headers = { Authorization: "Bearer " + token.uuid };
      this.setState({ loggedIn: false });
      axios
        .get(url, { headers })
        .then((response) => {
          console.log(response);
          alert(`${response.data}`);
          auth.logout(() => {
            this.setState({ loggedIn: false });
            this.props.history.push("/");
          });

          // this.setState({loggedIn:false});
        })
        .catch((error) => console.log(error));
    }
    localStorage.removeItem("token");
    //this.setState({loggedIn:false});
  }

  render() {
    if (!this.state.loggedIn) {
      console.log("loggedIn: ", this.state.loggedIn);
      return (
        <Router>
          <Redirect to="/" />
          <Switch>
            <Route exact path="/" component={SignIn} />
          </Switch>
        </Router>
      );
    } else {
      return (
        <Router>
          <MDBNavbar color="stylish-color-dark" dark expand="md">
            <MDBNavbarBrand>
              <MDBIcon icon="dolly" className="mr-2" />
              <strong className="white-text">Dalrada</strong>
            </MDBNavbarBrand>
            <MDBNavbarToggler onClick={this.toggleCollapse} />
            <MDBCollapse id="navbarCollapse3" isOpen={this.state.isOpen} navbar>
              <MDBNavbarNav right>
                <MDBNavItem>
                  <MDBNavLink className="white-text m-2" to="/home">
                    <MDBIcon icon="home" className="mr-2" />
                    Home
                  </MDBNavLink>
                </MDBNavItem>
                <MDBNavItem>
                  <MDBBtn className="white-text m-2">
                    <MDBIcon icon="smile" className="mr-2" />
                    Hi {this.state.userName}
                  </MDBBtn>
                </MDBNavItem>
                <MDBNavItem>
                  <MDBBtn
                    className="white-text m-2"
                    onClick={() => this.signOut()}
                  >
                    <MDBIcon icon="sign-in-alt" className="mr-2" />
                    Sign Out
                  </MDBBtn>
                </MDBNavItem>
              </MDBNavbarNav>
            </MDBCollapse>
          </MDBNavbar>

          {/* Nav */}

          <div className="row w-100  h-100 position-sticky pt-xs-5">
            <div className="col-xs-12 pt-xl-4 mt-xs-5 pt-xs-5 mb-5 pb-5 w-70 h-100 ">
              <div className="menu-bar">
                <input type="checkbox" id="check" />
                <label htmlFor="check" className="navBarLabel">
                  <div id="button">
                    <i className="fa fa-bars" />
                  </div>
                </label>
                <div className="menu-items">
                  <ul>
                    <li>
                      <a href="/upload" className="black-text ">
                        Upload
                      </a>
                    </li>
                    <li>
                      <a href="/reconcile" className="black-text">
                        Reconcile
                      </a>
                    </li>
                    <li>
                      <a href="/warehouse" className="black-text">
                        Warehouse
                      </a>
                    </li>
                    <li>
                      <a href="/user" className="black-text">
                        User
                      </a>
                    </li>
                    <li>
                      <a href="/role" className="black-text">
                        Role
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
          {/* End Nav */}

          <div className="mt-xl-3 mt-sm-1 pt-xl-3 pt-sm-1 col-xl-12 col-sm-12 mb-5 pb-5">
            <Switch>
              <Route exact path="/home" component={Logo} />
              <Route exact path="/upload" component={Upload} />
              <Route exact path="/reconcile" component={Reconcile} />
              <Route exact path="/warehouse" component={Warehouse} />
              <Route exact path="/user" component={User} />
              <Route exact path="/role" component={Role} />
              {/* <Route exact path="/signin" component={SignIn} /> */}
            </Switch>
          </div>
        </Router>
      );
    }
  }
}

export default Home;
