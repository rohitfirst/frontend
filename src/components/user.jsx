import React, { Component } from "react";
import axios from "axios";
import Select from "react-select";
import {
  MDBContainer,
  MDBBtn,
  MDBCard,
  MDBCardHeader,
  MDBCardBody,
  MDBIcon,
  MDBModal,
  MDBModalBody,
  MDBModalFooter,
  MDBDataTable,
  MDBRow,
  MDBCol,
  MDBLink,
  MDBInput,
} from "mdbreact";

const columns = [
  {
    label: "Id",
    field: "userId",
    sort: "asc",
    width: 40,
  },
  {
    label: "Name",
    field: "userName",
    sort: "asc",
    width: 100,
  },
  {
    label: "Email",
    field: "userEmail",
    sort: "asc",
    width: 220,
  },
  {
    label: "Password",
    field: "userPassword",
    sort: "asc",
    width: 120,
  },
  {
    label: "Role",
    field: "roleName",
    sort: "asc",
    width: 100,
  },

  {
    label: "Created Date",
    field: "createdDate",
    sort: "asc",
    width: 120,
  },
  {
    label: "CreatedBy",
    field: "createdBy",
    sort: "asc",
    width: 100,
  },

  {
    label: "Status",
    field: "status",
    sort: "asc",
    width: 120,
  },
  {
    label: "Edit",
    field: "edit",
    sort: "asc",
    width: 100,
  },
];
const rows = [];

class User extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pagingAndSearching: false,
      username: "",
      userId: "",
      email: "",
      password: "",
      role: "",
      editUsername: "",
      editEmail: "",
      editPassword: "",
      editRole: "",
      editUserId: "",
      editStatus: "",
      editCreatedDate: "",
      editCreatedBy: "",
      editRole: "",
      modal: false,
      roleItems: [],
      usernameError: "",
      passwordError: "",
      emailError: "",
      roleError: "",
      data: { columns, rows },
    };
  }
  toggle = () => {
    this.setState({
      modal: !this.state.modal,
    });
  };
  componentDidMount() {
    this.getRoles();
    this.getUsers();
  }
  handleUsernameChange = (event) => {
    this.setState({
      username: event.target.value,
    });
  };

  handlePasswordChange = (event) => {
    this.setState({
      password: event.target.value,
    });
  };
  handleEmailChange = (event) => {
    this.setState({
      email: event.target.value,
    });
  };
  handleRoleChange = (event) => {
    this.setState({
      role: event.target.value,
    });
  };

  handleEditUsernameChange = (event) => {
    this.setState({
      editUsername: event.target.value,
    });
  };

  handleEditPasswordChange = (event) => {
    this.setState({
      editPassword: event.target.value,
    });
  };
  handleEditEmailChange = (event) => {
    this.setState({
      editEmail: event.target.value,
    });
  };
  handleEditRoleChange = (event) => {
    this.setState({
      editRole: event.target.value,
    });
  };

  reset = (event) => {
    this.setState({
      username: "",
      userId: "",
      email: "",
      password: "",
      role: "",
      editUsername: "",
      editEmail: "",
      editPassword: "",
      editRole: "",
      editUserId: "",
      editStatus: "",
      editCreatedDate: "",
      editCreatedBy: "",
      editRole: "",
      usernameError: "",
      passwordError: "",
      emailError: "",
      roleError: "",
      editUsernameError: "",
      editPasswordError: "",
      editEmailError: "",
      editRoleError: "",
    });
  };

  resetError(event) {
    this.setState({
      usernameError: "",
      passwordError: "",
      emailError: "",
      roleError: "",
      editUsernameError: "",
      editPasswordError: "",
      editEmailError: "",
      editRoleError: "",
    });
  }

  validate = () => {
    this.resetError();
    const passwordRegex = /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,16})/;
    const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    const usernameRegex = /^[a-zA-Z ]*$/;
    const rows = this.state.data.rows;
    console.log(rows);
    let userNameFound = rows.find((item) => {
      return item.userName === this.state.username;
    });
    let emailFound = rows.find((item) => {
      return item.userEmail === this.state.email;
    });
    if (
      !this.state.username &&
      !this.state.email &&
      !this.state.role &&
      !this.state.password
    ) {
      this.setState({
        usernameError: "UserName cannot be Empty",
        emailError: "Email is Required",
        roleError: "Please  Select  Role",
        passwordError: "Password cannot be Empty",
      });
      return false;
    } else if (!this.state.username) {
      this.setState({
        usernameError: "username is mandatory",
      });
      return false;
    } else if (!usernameRegex.test(this.state.username)) {
      this.setState({
        usernameError: "username should contain only alphabets",
      });
      return false;
    } else if (this.state.username.length < 3) {
      this.setState({
        usernameError: "username should contain morethan 3 letters",
      });
      return false;
    } else if (userNameFound) {
      this.setState({
        usernameError: "Username already exists",
      });
      return false;
    }

    if (!this.state.email) {
      this.setState({ emailError: "Email-Id is Required" });
      return false;
    } else if (!emailRegex.test(this.state.email)) {
      this.setState({
        emailError: "Invalid Email-Id",
      });
      return false;
    } else if (emailFound) {
      this.setState({
        emailError: "Email-ID already exists",
      });
      return false;
    }

    if (!this.state.password) {
      this.setState({ passwordError: "Password is Required" });
      return false;
    } else if (this.state.password.length < 8) {
      this.setState({ passwordError: "Password must be 8 characters long." });
      return false;
    } else if (!passwordRegex.test(this.state.password)) {
      this.setState({
        passwordError: "Password Format : A2@qwerr",
      });
      return false;
    }

    if (!this.state.role.value) {
      this.setState({ roleError: "Please select role" });
      return false;
    }

    return true;
  };

  validateEdit = () => {
    this.resetError();
    const passwordRegex = /(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,16})/;
    const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    const usernameRegex = /^[a-zA-Z ]*$/;
    const rows = this.state.data.rows;
    console.log(rows);
    let userNameFound = rows.find((item) => {
      return item.userName === this.state.editUsername;
    });
    let emailFound = rows.find((item) => {
      return item.userEmail === this.state.editEmail;
    });
    if (!this.state.editUsername) {
      this.setState({
        editUsernameError: "username is mandatory",
      });
      return false;
    } else if (!usernameRegex.test(this.state.editUsername)) {
      this.setState({
        editUsernameError: "username should contain only alphabets",
      });
      return false;
    } else if (this.state.editUsername.length < 3) {
      this.setState({
        editUsernameError: "username should contain morethan 3 letters",
      });
      return false;
    } else if (userNameFound) {
      this.setState({
        editUsernameError: "User name already exists",
      });
      return false;
    }

    if (!this.state.editEmail) {
      this.setState({ editEmailError: "Email-Id is Required" });
      return false;
    } else if (!emailRegex.test(this.state.editEmail)) {
      this.setState({
        editEmailError: "Invalid Email-Id",
      });
      return false;
    } else if (emailFound) {
      this.setState({
        editEmailError: "Email already exists",
      });
      return false;
    }

    if (!this.state.editPassword) {
      this.setState({ editPasswordError: "Password is Required" });
      return false;
    } else if (this.state.editPassword.length < 8) {
      this.setState({
        editPasswordError: "Password must be 8 characters long.",
      });
      return false;
    } else if (!passwordRegex.test(this.state.editPassword)) {
      this.setState({
        editPasswordError: "Password Format: A2@qwerr",
      });
      return false;
    }

    if (!this.state.editRole.value) {
      this.setState({ editRoleError: "Please select role" });
      return false;
    }

    return true;
  };

  handleSubmit = (event) => {
    event.preventDefault();
    const isValid = this.validate();
    if (isValid) {
      event.preventDefault();
      this.createUser();
      // clear form
      this.reset();
    }
  };

  handleEditSubmit = (event) => {
    event.preventDefault();
    const isValid = this.validateEdit();
    if (isValid) {
      event.preventDefault();
      this.editUser();
      // clear form
      this.setState({
        username: "",
        email: "",
        password: "",
        role: "",
        usernameError: "",
        passwordError: "",
        roleError: "",
      });
      this.toggle();
    }
  };

  async editUser() {
    const url = "dalrada/user/userResource/users/edit";
    const request = {
      userId: this.state.editUserId,
      userName: this.state.editUsername,
      userEmail: this.state.editEmail,
      userPassword: this.state.editPassword,
      status: this.state.editStatus,
      createdDate: this.state.editCreatedDate,
      createdBy: this.state.editCreatedBy,
      roleName: this.state.editRole.value,
    };
    console.log(request);
    axios
      .post(url, request)
      .then((response) => {
        const user = response.data.respBody;
        user.edit = (
          <MDBBtn
            color="primary"
            size="sm"
            className="text-center rounded-pill"
            onClick={() => {
              this.editForm(response.data.respBody);
            }}
          >
            Edit
          </MDBBtn>
        );
        if (response.data.respBody.status === 1)
          user.status = (
            <MDBBtn
              color="success"
              size="sm"
              clssName="text-center rounded-pill"
            >
              Active
            </MDBBtn>
          );
        if (response.data.respBody.status === 0)
          user.status = (
            <MDBBtn
              color="danger"
              size="sm"
              clssName="text-center rounded-pill"
            >
              Inactive
            </MDBBtn>
          );
        this.state.data.rows.push(user);
        this.getUsers();
      })
      .catch((error) => console.log(error));
  }

  async createUser() {
    const url = "dalrada/user/userResource/users/create";
    let token = JSON.parse(localStorage.getItem("token"));
    const request = {
      userName: this.state.username,
      userEmail: this.state.email,
      userPassword: this.state.password,
      status: 1,
      createdDate: new Date(),
      createdBy: token.username,
      roleName: this.state.role.value,
    };
    console.log(request);
    axios
      .post(url, request)
      .then((response) => {
        const role = response.data.respBody;
        role.edit = (
          <MDBBtn
            color="primary"
            size="sm"
            className="text-center rounded-pill"
            onClick={() => {
              this.editForm(response.data.respBody);
            }}
          >
            Edit
          </MDBBtn>
        );
        if (response.data.respBody.status === 1)
          role.status = (
            <MDBBtn
              color="success"
              size="sm"
              className="text-center rounded-pill"
              onClick={() => {
                this.editStatus(response.data.respBody);
              }}
            >
              Active
            </MDBBtn>
          );
        if (response.data.respBody.status === 0)
          role.status = (
            <MDBBtn
              color="danger"
              size="sm"
              className="text-center rounded-pill"
              onClick={() => {
                this.editStatus(response.data.respBody);
              }}
            >
              Inactive
            </MDBBtn>
          );
        this.state.data.rows.push(role);
        alert(`User record is created successfully.`);
        this.getUsers();
      })
      .catch((error) => {
        alert(`Something is wrong , Try it again.`);
        console.log(error);
      });
  }

  async editStatus(user) {
    console.log(user);
    const userId = user.userId;
    const status = user.status.props.children === "Active" ? 0 : 1;

    const url = "dalrada/user/userResource/users/" + userId + "/" + status;
    axios
      .post(url)
      .then((response) => {
        const role = response.data.respBody;
        role.edit = (
          <MDBBtn
            color="primary"
            size="sm"
            className="text-center rounded-pill"
            onClick={() => {
              this.editForm(response.data.respBody);
            }}
          >
            Edit
          </MDBBtn>
        );
        if (response.data.respBody.status === 1)
          role.status = (
            <MDBBtn
              color="success"
              size="sm"
              className="text-center rounded-pill"
              onClick={() => {
                this.editStatus(response.data.respBody);
              }}
            >
              Active
            </MDBBtn>
          );
        if (response.data.respBody.status === 0)
          role.status = (
            <MDBBtn
              color="danger"
              size="sm"
              className="text-center rounded-pill"
              onClick={() => {
                this.editStatus(response.data.respBody);
              }}
            >
              Inactive
            </MDBBtn>
          );
        this.state.data.rows.push(role);
        alert(`Status is updated successfully.`);
        this.getUsers();
      })
      .catch((error) => {
        alert(`Something is wrong , Try it again.`);
        console.log(error);
      });
  }

  handleChange = (selectedOption) => {
    this.setState({ role: selectedOption });
  };
  handleEditChange = (selectedOption) => {
    this.setState({ editRole: selectedOption });
  };

  async getUsers() {
    const url = "dalrada/user/userResource/users";
    axios
      .get(url)
      .then((response) => {
        console.log(response.data);
        let rows = response.data.map((item) => {
          const user = item.respBody;
          user.edit = (
            <MDBBtn
              color="primary"
              size="sm"
              className="text-center rounded-pill"
              onClick={() => {
                this.editForm(item.respBody);
              }}
            >
              Edit
            </MDBBtn>
          );
          if (item.respBody.status === 1)
            user.status = (
              <MDBBtn
                color="success"
                size="sm"
                className="text-center rounded-pill"
                onClick={() => {
                  this.editStatus(item.respBody);
                }}
              >
                Active
              </MDBBtn>
            );
          if (item.respBody.status === 0)
            user.status = (
              <MDBBtn
                color="danger"
                size="sm"
                className="text-center rounded-pill"
                onClick={() => {
                  this.editStatus(item.respBody);
                }}
              >
                Inactive
              </MDBBtn>
            );
          return user;
        });
        if (rows.lenth > 10) {
          this.setState({
            pagingAndSearching: true,
          });
        }
        this.setState({
          data: { columns, rows },
        });
      })
      .catch((error) => console.log(error));
  }

  async editForm(user) {
    this.setState({
      editUsername: user.userName,
      editEmail: user.userEmail,
      editPassword: user.userPassword,
      editRole: user.roleName,
      editUserId: user.userId,
      editStatus: user.status.props.children === "Active" ? 1 : 0,
      editCreatedDate: user.createdDate,
      editCreatedBy: user.createdBy,
    });
    this.toggle();
  }

  async getRoles() {
    axios
      .get("/dalrada/user/roleResource/roles")
      .then((response) => {
        const roles = response.data
          .filter((item) => item.respBody.status === 1)
          .map((resp) => {
            return {
              value: resp.respBody.roleName,
              label: resp.respBody.roleName,
            };
          });

        this.setState({
          roleItems: roles,
        });
      })
      .catch((error) => console.log(error));
  }

  render() {
    return (
      <MDBContainer>
        <div>
          <div>
            <MDBCard>
              <MDBCardHeader
                titleClass="d-inline title"
                color="brown lighten-5"
                type="text"
                className="text-center  darken-3 white-text"
              >
                Create User
              </MDBCardHeader>
              <MDBCardBody>
                <form onSubmit={this.handleSubmit}>
                  <MDBContainer>
                    <MDBRow>
                      <MDBCol md="6">
                      <MDBInput label="User Name:" 
                                type="text"
                                value={this.state.username}
                                onChange={this.handleUsernameChange}
                                iconClass="dark-grey"
                                className="text-center form-control"/>
                        <div className="text-center red-text">
                          {this.state.usernameError}
                        </div>        
                        {/* <label htmlFor="formGroupExampleInput">
                          User Name:<span style={{ color: "red" }}>*</span>
                        </label>
                        <input
                          type="text"
                          value={this.state.username}
                          onChange={this.handleUsernameChange}
                          iconClass="dark-grey"
                          className="text-center form-control"
                        />
                        <div className="text-center red-text">
                          {this.state.usernameError}
                        </div> */}
                      </MDBCol>
                      <MDBCol md="6">
                      <MDBInput label="Email:" 
                                type="text"
                                value={this.state.email}
                                onChange={this.handleEmailChange}
                                iconClass="dark-grey"
                                className="text-center form-control"/>
                        <div className="text-center red-text">
                          {this.state.emailError}
                        </div>        
                        {/* <label htmlFor="formGroupExampleInput">
                          Email:<span style={{ color: "red" }}>*</span>
                        </label>
                        <input
                          type="text"
                          value={this.state.email}
                          onChange={this.handleEmailChange}
                          iconClass="dark-grey"
                          className="text-center form-control"
                        />
                        <div className="text-center red-text">
                          {this.state.emailError}
                        </div> */}
                      </MDBCol>
                    </MDBRow>
                    <br></br>
                    <MDBRow>
                      <MDBCol md="6">
                        <label htmlFor="formGroupExampleInput">
                          Role:<span style={{ color: "red" }}>*</span>
                        </label>
                        <Select
                          value={this.state.role}
                          onChange={this.handleChange}
                          options={this.state.roleItems}
                        />
                        <div className="text-center red-text">
                          {this.state.roleError}
                        </div>
                      </MDBCol>
                      <MDBCol md="6">
                      <MDBInput label="Password:" 
                                type="text"
                                value={this.state.password}
                                onChange={this.handlePasswordChange}
                                iconClass="dark-grey"
                                className="text-center form-control"/>
                        <div className="text-center red-text">
                          {this.state.passwordError}
                        </div>        
                        {/* <label htmlFor="formGroupExampleInput">
                          Password:<span style={{ color: "red" }}>*</span>
                        </label>
                        <input
                          type="text"
                          value={this.state.password}
                          onChange={this.handlePasswordChange}
                          iconClass="dark-grey"
                          className="text-center form-control"
                        />
                        <div className="text-center red-text">
                          {this.state.passwordError}
                        </div> */}
                      </MDBCol>
                    </MDBRow>
                  </MDBContainer>
                  <MDBContainer>
                    <MDBRow>
                      <MDBCol md="12">
                          <span style={{color:"grey lighten-3",fontSize:"15px",fontWeight:"5"}}>NOTE:All field are mandatory</span>
                      </MDBCol>
                    </MDBRow>
                  </MDBContainer>
                  <div className="text-center mt-1-half">
                    <MDBBtn
                      color="primary"
                      className="mb-2 mt-3 rounded-pill"
                      size="sm"
                      type="submit"
                    >
                      Create
                      <MDBIcon icon="paper-plane" className="ml-1" />
                    </MDBBtn>
                    <MDBBtn
                      color="danger"
                      className="mb-2 mt-3 rounded-pill"
                      size="sm"
                      type="reset"
                      onClick={this.reset}
                    >
                      Reset
                    </MDBBtn>
                  </div>
                </form>
              </MDBCardBody>
            </MDBCard>
          </div>
          <div className="mt-5">
            <MDBCard>
              <MDBCardHeader
                titleClass="d-inline title"
                color="brown lighten-5"
                className="text-center darken-3 white-text"
              >
                Manage User
              </MDBCardHeader>
              <MDBCardBody>
                <MDBDataTable
                  striped
                  bordered
                  responsive
                  fixed={true}
                  maxHeight="700px"
                  searchLabel=""
                  small
                  entriesLabel=""
                  noBottomColumns={true}
                  scrollY={true}
                  btn={true}
                  autoWidth={true}
                  paging={this.state.pagingAndSearching}
                  searching={this.state.pagingAndSearching}
                  data={this.state.data}
                />
              </MDBCardBody>
            </MDBCard>
          </div>
          <MDBModal isOpen={this.state.modal} toggle={this.toggle}>
            <MDBModalBody>
              <MDBCard>
                <MDBCardHeader
                  titleClass="d-inline title"
                  color="brown lighten-5"
                  type="text"
                  className="text-center  darken-3 white-text"
                >
                  Edit User
                  <button
                    variant="contained"
                    style={{ float: "right" }}
                    color="brown lighten-5"
                    className="float-right"
                    onClick={this.toggle}
                  >
                    <MDBIcon icon="times" size="lg" className="red-text " />
                  </button>
                </MDBCardHeader>
                <MDBCardBody>
                  <form onSubmit={this.handleEditSubmit}>
                    <MDBContainer>
                      <MDBRow>
                        <MDBCol md="6">
                        <MDBInput label="User Name:" 
                                  type="text"
                                  value={this.state.editUsername}
                                  onChange={this.handleEditUsernameChange}
                                  iconClass="dark-grey"
                                  className="form-control text-center"/>
                          <div className="text-center red-text">
                            {this.state.editUsernameError}
                          </div>        
                          {/* <label htmlFor="formGroupExampleInput">
                            User Name:<span style={{ color: "red" }}>*</span>
                          </label>
                          <input
                            type="text"
                            value={this.state.editUsername}
                            onChange={this.handleEditUsernameChange}
                            iconClass="dark-grey"
                            className="form-control text-center"
                          />
                          <div className="text-center red-text">
                            {this.state.editUsernameError}
                          </div> */}
                        </MDBCol>
                        <MDBCol md="6">
                        <MDBInput label="Email:" 
                                  type="text"
                                  value={this.state.editEmail}
                                  onChange={this.handleEditEmailChange}
                                  iconClass="dark-grey"
                                  className="form-control text-center"/>
                          <div className="text-center red-text">
                            {this.state.editEmailError}
                          </div>        
                          {/* <label htmlFor="formGroupExampleInput">
                            Email:<span style={{ color: "red" }}>*</span>
                          </label>
                          <input
                            type="text"
                            value={this.state.editEmail}
                            onChange={this.handleEditEmailChange}
                            iconClass="dark-grey"
                            className="form-control text-center"
                          />
                          <div className="text-center red-text">
                            {this.state.editEmailError}
                          </div> */}
                        </MDBCol>
                      </MDBRow>
                      <MDBRow>
                        <MDBCol md="6">
                          <label htmlFor="formGroupExampleInput">
                            Role:<span style={{ color: "red" }}>*</span>
                          </label>
                          <Select
                            value={this.state.editRole}
                            onChange={this.handleEditChange}
                            options={this.state.roleItems}
                          />
                          <div className="text-center red-text">
                            {this.state.editRoleError}
                          </div>
                        </MDBCol>
                        <MDBCol md="6">
                        <MDBInput label="Password:" 
                                  type="text"
                                  value={this.state.editPassword}
                                  onChange={this.handleEditPasswordChange}
                                  iconClass="dark-grey"
                                  className="form-control text-center"/>
                          <div className="text-center red-text">
                            {this.state.editPasswordError}
                          </div>        
                          {/* <label htmlFor="formGroupExampleInput">
                            Password:<span style={{ color: "red" }}>*</span>
                          </label>
                          <input
                            type="text"
                            value={this.state.editPassword}
                            onChange={this.handleEditPasswordChange}
                            iconClass="dark-grey"
                            className="form-control text-center"
                          />
                          <div className="text-center red-text">
                            {this.state.editPasswordError}
                          </div> */}
                        </MDBCol>
                      </MDBRow>
                    </MDBContainer>
                    <MDBContainer>
                    <MDBRow>
                      <MDBCol md="12">
                          <span style={{color:"grey lighten-3",fontSize:"15px",fontWeight:"5"}}>NOTE:All field are mandatory</span>
                      </MDBCol>
                    </MDBRow>
                  </MDBContainer>
                    <div className="text-center mt-1-half">
                      <MDBBtn
                        color="primary"
                        className="mb-2 mt-3 rounded-pill"
                        size="sm"
                        type="submit"
                      >
                        SUBMIT
                        <MDBIcon icon="paper-plane" className="ml-1" />
                      </MDBBtn>
                      <MDBBtn
                        color="danger"
                        className="mb-2 mt-3 rounded-pill"
                        size="sm"
                        type="reset"
                        onClick={this.reset}
                      >
                        RESET
                      </MDBBtn>
                    </div>
                  </form>
                </MDBCardBody>
              </MDBCard>
            </MDBModalBody>
          </MDBModal>
        </div>
      </MDBContainer>
    );
  }
}

export default User;
