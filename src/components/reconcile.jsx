import React, { Component } from "react";
import axios from "axios";
import {
  MDBContainer,
  MDBBtn,
  MDBCard,
  MDBCardHeader,
  MDBCardBody,
  MDBIcon,
  MDBDataTable,
  MDBRow,
  MDBCol,
  MDBLink,
  MDBTable,
  MDBModal,
  MDBModalBody,
  MDBTableBody,
  MDBModalFooter,
} from "mdbreact";
import DatePicker from "react-datepicker";
import Loader from "react-loader-spinner";
import "react-datepicker/dist/react-datepicker.css";
import "./reconcile.css";
const columns = [
  {
    label: (
      <div className="text-center" style={{ width: "175px" }}>
        Order Date <MDBIcon className="ml-4" icon="sort" />
      </div>
    ),
    field: "orderDate",
    sort: "asc",
    width: 180,
  },

  {
    label: (
      <div className="text-center" style={{ width: "125px" }}>
        <div>Po</div>
        <span>
          Number
          <MDBIcon className="ml-2" icon="sort" />
        </span>
      </div>
    ),
    field: "poNumber",
    width: 130,
  },
  {
    label: "",
    field: "select",
    width: 150,
  },
  {
    label: (
      <div className="text-center" style={{ width: "145px" }}>
        <div>Channel</div>
        <span>
          Name
          <MDBIcon className="ml-2" icon="sort" />
        </span>
      </div>
    ),
    field: "channelName",
    sort: "asc",
    width: 130,
  },
  {
    label: (
      <div className="text-center" style={{ width: "126px" }}>
        <div>Order </div>
        <span>
          Status
          <MDBIcon className="ml-2" icon="sort" />
        </span>
      </div>
    ),
    field: "orderStatus",
    sort: "asc",
    width: 100,
  },
  {
    label: (
      <div className="text-center" style={{ width: "95px" }}>
        <span>
          SKU
          <MDBIcon className="ml-2" icon="sort" />
        </span>
      </div>
    ),
    field: "sku",
    sort: "asc",
    width: 400,
  },
  {
    label: (
      <div className="text-center" style={{ width: "400px" }}>
        <span>
          Product Name
          <MDBIcon className="ml-2" icon="sort" />
        </span>
      </div>
    ),
    field: "productName",
    sort: "asc",
    width: 84,
  },

  {
    label: (
      <div className="text-center" style={{ width: "80px" }}>
        <span>
          Quantity
          <MDBIcon className="ml-2" icon="sort" />
        </span>
      </div>
    ),
    field: "quantity",
    sort: "asc",
    width: 86,
  },
  {
    label: (
      <div className="text-center" style={{ width: "80px" }}>
        <div>Order </div>
        <span>
          Total
          <MDBIcon className="ml-2" icon="sort" />
        </span>
      </div>
    ),
    field: "orderTotal",
    sort: "asc",
    width: 90,
  },
  {
    label: (
      <div className="text-center" style={{ width: "80px" }}>
        <div>Supplier Cost</div>
        <span>
          Per SKU
          <MDBIcon className="ml-2" icon="sort" />
        </span>
      </div>
    ),
    field: "supplierCostPerSKU",
    sort: "asc",
    width: 85,
  },
  {
    label: (
      <div className="text-center" style={{ width: "80px" }}>
        <div>Sale </div>
        <span>
          Price
          <MDBIcon className="ml-2" icon="sort" />
        </span>
      </div>
    ),
    field: "salePrice",
    sort: "asc",
    width: 87,
  },
  {
    label: (
      <div className="text-center" style={{ width: "80px" }}>
        <div>Amazon</div>
        <span>
          Amount
          <MDBIcon className="ml-2" icon="sort" />
        </span>
      </div>
    ),
    field: "amazonAmount",
    sort: "asc",
    width: 107,
  },
  {
    label: (
      <div className="text-center" style={{ width: "100px" }}>
        <span>
          ExtNetUnit
          <MDBIcon className="ml-2" icon="sort" />
        </span>
      </div>
    ),
    field: "extNetUnit",
    sort: "asc",
    width: 87,
  },
  {
    label: (
      <div className="text-center" style={{ width: "80px" }}>
        <span>
          Profit
          <MDBIcon className="ml-2" icon="sort" />
        </span>
      </div>
    ),
    field: "profit",
    sort: "asc",
    width: 89,
  },

  {
    label: (
      <div className="text-center" style={{ width: "80px" }}>
        <span>
          ROI
          <MDBIcon className="ml-2" icon="sort" />
        </span>
      </div>
    ),
    field: "roi",
    sort: "asc",
    width: 85,
  },
  {
    label: (
      <div className="text-center" style={{ width: "77px" }}>
        <div>Average</div>
        <span>
          Profit
          <MDBIcon className="ml-2" icon="sort" />
        </span>
      </div>
    ),
    field: "avgProfit",
    sort: "asc",
    width: 80,
  },
];
const singleQuantityRows = [];
const multiQuantityRows = [];

class Reconcile extends Component {
  state = {
    startDate: "",
    endDate: "",
    dateError: "",
    modal: false,
    showReconcile: true,
    reconcileLoader: false,
    singleQuantityData: { columns, singleQuantityRows },
    multiQuantityData: { columns, multiQuantityRows },
  };
  toggle = () => {
    this.setState({
      modal: !this.state.modal,
    });
  };
  handleStartDateChange = (date) => {
    this.setState({
      startDate: date,
    });
  };
  handleEndDateChange = (date) => {
    this.setState({
      endDate: date,
    });
  };

  validateOrders = () => {
    const from = this.state.startDate;
    const to = this.state.endDate;

    if (!from) {
      this.setState({
        dateError: "Enter the Start DATE",
      });
      return false;
    }
    if (!to) {
      this.setState({
        dateError: "Enter the END DATE",
      });
      return false;
    }
    return true;
  };

  resetDates = (event) => {
    this.setState({
      orderFile: "",
      startDate: "",
      endDate: "",
      dateError: "",
      orderDate: "",
      poNumber: "",
      channelName: "",
      orderStatus: "",
      sku: "",
      productName: "",
      quantity: "",
      supplierCostPerSKU: "",
      salePrice: "",
      amazonAmount: "",
      extNetUnit: "",
      profit: "",
      roi: "",
      avgProfit: "",
      showDownloadButton: true,
    });
  };

  handleOrderSubmit = (event) => {
    event.preventDefault();
    const isValid = this.validateOrders();
    if (isValid) {
      const from = new Date(
        this.state.startDate.getFullYear(),
        this.state.startDate.getMonth(),
        this.state.startDate.getDate() + 1
      );
      const to = new Date(
        this.state.endDate.getFullYear(),
        this.state.endDate.getMonth(),
        this.state.endDate.getDate() + 1
      );

      const startDate = from.toISOString().split("T")[0];
      const endDate = to.toISOString().split("T")[0];
      const url = "/dalrada/reconcile/orders/" + startDate + "/" + endDate;
      this.getData(url);
      this.resetDates();
    }
  };

  async getData(url) {
    this.setState({
      reconcileLoader: true,
    });
    let token = JSON.parse(localStorage.getItem("token"));
    const headers = { Authorization: "Bearer " + token.uuid };
    console.log(`${headers.Authorization}`);
    axios
      .get(url + "/" + token.uuid, { headers })
      .then((response) => {
        let singleQuantityRows = response.data.singleQuantity.map((item) => {
          let order = item.respBody;
          order.select = (
            <span
              onClick={() => {
                this.showDetails(item.respBody);
              }}
            >
              {order.poNumber}
            </span>
          );

          return order;
        });
        let multiQuantityRows = response.data.multiQuantity.map((item) => {
          let order = item.respBody;
          order.select = (
            <span
              onClick={() => {
                this.showDetails(item.respBody);
              }}
            >
              {order.poNumber}
            </span>
          );
          return order;
        });

        this.setState({
          singleQuantityData: { columns, rows: singleQuantityRows },
          multiQuantityData: { columns, rows: multiQuantityRows },
          showReconcile: false,
          reconcileLoader: false,
        });
      })
      .catch((error) => {
        this.setState({
          reconcileLoader: false,
        });
        console.log(error);
      });
  }

  async showDetails(order) {
    this.setState({
      orderDate: order.orderDate,
      poNumber: order.poNumber,
      channelName: order.channelName,
      orderStatus: order.orderStatus,
      sku: order.sku,
      productName: order.productName,
      quantity: order.quantity,
      orderTotal: order.orderTotal,
      supplierCostPerSKU: order.supplierCostPerSKU,
      salePrice: order.salePrice,
      amazonAmount: order.amazonAmount,
      extNetUnit: order.extNetUnit,
      profit: order.profit,
      roi: order.roi,
      avgProfit: order.avgProfit,
    });
    this.toggle();
  }

  hasSortBy() {
    var sortObj = {};
    var th = "";
    var hasSortByAsc = document.getElementsByClassName("sorting_asc");
    var hasSortByDesc = document.getElementsByClassName("sorting_desc");
    if (hasSortByAsc.length > 0) {
      th = hasSortByAsc[0].innerText;
      sortObj.sortBy = th.replace(/\s+/g, "").toLowerCase();
      sortObj.type = "asc";
    } else if (hasSortByDesc.length > 0) {
      th = hasSortByDesc[0].innerText;
      sortObj.sortBy = th.replace(/\s+/g, "").toLowerCase();
      sortObj.type = "desc";
    }
    return sortObj;
  }

  downloadFile() {
    var sortStatus = this.hasSortBy();
    ///alert(sortStatus.length);
    const url =
      sortStatus.type !== undefined
        ? "/dalrada/reconcile/orders/download/" +
          sortStatus.sortBy +
          "/" +
          sortStatus.type
        : "/dalrada/reconcile/orders/download/default/asc";

    console.log(url);

    let token = JSON.parse(localStorage.getItem("token"));
    const headers = { Authorization: "Bearer " + token.uuid };
    console.log(`${headers.Authorization}`);
    axios
      .get(url + "/" + token.uuid, { headers, responseType: "blob" })
      .then((response) => {
        const url = window.URL.createObjectURL(new Blob([response.data]));
        const link = document.createElement("a");
        link.href = url;
        link.setAttribute("download", "file.csv");
        document.body.appendChild(link);
        link.click();
        // console.log(response);
        // if (response === 200) {
        //   alert("200  response");
        // }
      })
      .catch((error) => console.log(error));
  }
  render() {
    const today = new Date();
    return (
      <MDBContainer>
        <div className="row">
          <div className="col-xl-12 col-sm-12">
            <MDBCard hidden={!this.state.showReconcile}>
              <MDBCardHeader
                titleClass="d-inline title"
                color="brown lighten-5"
                className="text-center darken-3 white-text"
              >
                Order Details
              </MDBCardHeader>
              <MDBCardBody>
                <form onSubmit={this.handleOrderSubmit}>
                  <MDBRow>
                    <MDBCol md="6">
                      <div className="row mt-3">
                        <span className="col-4">
                          From Date:<span style={{ color: "red" }}>*</span>
                        </span>
                        <DatePicker
                          className="mr-5 col-8"
                          showMonthDropdown
                          showYearDropdown
                          selected={this.state.startDate}
                          value={this.state.startDate}
                          type="date"
                          maxDate={today}
                          dateFormat="MM-dd-yyyy"
                          onChange={this.handleStartDateChange}
                        />
                      </div>
                    </MDBCol>
                    <MDBCol md="6">
                      <div className="row mt-3">
                        <span className="col-3">
                          To Date:<span style={{ color: "red" }}>*</span>
                        </span>
                        <DatePicker
                          className="mr-5 col-8"
                          showMonthDropdown
                          showYearDropdown
                          selected={this.state.endDate}
                          value={this.state.endDate}
                          type="date"
                          maxDate={today}
                          minDate={this.state.startDate}
                          dateFormat="MM-dd-yyyy"
                          onChange={this.handleEndDateChange}
                        />
                      </div>
                    </MDBCol>
                  </MDBRow>

                  <div className="text-center red-text">
                    {this.state.dateError}
                  </div>
                  <div className="text-center mt-1-half">
                    <MDBBtn
                      color="primary"
                      className="mb-2 mt-3 rounded-pill"
                      size="sm"
                      type="submit"
                    >
                      Order Details
                      <MDBIcon icon="paper-plane" className="ml-1" />
                    </MDBBtn>
                    <MDBBtn
                      color="danger"
                      className="mb-2 mt-3 rounded-pill"
                      size="sm"
                      onClick={this.resetDates}
                    >
                      Reset
                    </MDBBtn>
                  </div>
                </form>
                <div className="text-center">
                  <span className="mt-5">
                    <Loader
                      visible={this.state.reconcileLoader}
                      type="ThreeDots"
                      color="#ff4444"
                      height={30}
                      width={80}
                    />
                  </span>
                </div>
              </MDBCardBody>
            </MDBCard>
          </div>
        </div>
        <div className="mt-5 reconcile-table">
          <MDBCard hidden={this.state.showReconcile}>
            <MDBCardHeader
              titleClass="d-inline title"
              color="brown lighten-5"
              className="text-center darken-3 white-text"
            >
              Reconcile Report
            </MDBCardHeader>
            <MDBCardBody className="p-4 w-5">
              <div className="text-right">
                <MDBBtn
                  color="success"
                  className="mb-2 mt-3  rounded-pill"
                  size="sm"
                  type="submit"
                  hidden={this.state.showReconcile}
                  onClick={this.downloadFile.bind(this)}
                >
                  Download
                  <MDBIcon icon="paper-plane" className="ml-1" />
                </MDBBtn>
              </div>
              <div>
                <MDBDataTable
                  bordered
                  responsive
                  fixed={true}
                  maxHeight="400px"
                  searchLabel=""
                  small
                  entriesLabel=""
                  paging={true}
                  searching={true}
                  order={["srNo", "asc"]}
                  data={this.state.singleQuantityData}
                  className="table single-reconcile"
                  noBottomColumns={true}
                  autoWidth={true}
                  scrollX={true}
                />
              </div>
              <div>
                <MDBDataTable
                  bordered
                  responsive
                  fixed={true}
                  maxHeight="400px"
                  searchLabel=""
                  small
                  entriesLabel=""
                  paging={true}
                  searching={true}
                  order={["srNo", "asc"]}
                  data={this.state.multiQuantityData}
                  className="table multi-reconcile"
                  noBottomColumns={true}
                  autoWidth={true}
                  scrollX={true}
                />
              </div>
            </MDBCardBody>
          </MDBCard>
        </div>
        <MDBModal isOpen={this.state.modal} toggle={this.toggle}>
          <MDBModalBody>
            <MDBCard>
              <MDBCardHeader
                titleClass="d-inline title"
                color="brown lighten-5"
                type="text"
                className="text-center  darken-3 white-text"
              >
                Order Record
                <button
                  variant="contained"
                  style={{ float: "right" }}
                  color="brown lighten-5"
                  className="float-right"
                  onClick={this.toggle}
                >
                  <MDBIcon icon="times" size="lg" className="red-text " />
                </button>
              </MDBCardHeader>
              <MDBCardBody>
                <MDBTable bordered small>
                  <MDBTableBody>
                    <tr>
                      <td>Order Date</td>
                      <td>{this.state.orderDate}</td>
                    </tr>
                    <tr>
                      <td>Po Number</td>
                      <td>{this.state.poNumber}</td>
                    </tr>
                    <tr>
                      <td>Channel Name</td>
                      <td>{this.state.channelName}</td>
                    </tr>
                    <tr>
                      <td>Order Status</td>
                      <td>{this.state.orderStatus}</td>
                    </tr>
                    <tr>
                      <td>SKU</td>
                      <td>{this.state.sku}</td>
                    </tr>
                    <tr>
                      <td>Product Name</td>
                      <td>{this.state.productName}</td>
                    </tr>
                    <tr>
                      <td>Quantity</td>
                      <td>{this.state.quantity}</td>
                    </tr>
                    <tr>
                      <td>Order Total</td>
                      <td>{this.state.orderTotal}</td>
                    </tr>
                    <tr>
                      <td>Supplier Cost Per SKU</td>
                      <td>{this.state.supplierCostPerSKU}</td>
                    </tr>
                    <tr>
                      <td>Sales Price</td>
                      <td>{this.state.salePrice}</td>
                    </tr>
                    <tr>
                      <td>Amazon Amount</td>
                      <td>{this.state.amazonAmount}</td>
                    </tr>
                    <tr>
                      <td>Ext Net Unit</td>
                      <td>{this.state.extNetUnit}</td>
                    </tr>
                    <tr>
                      <td>Profit</td>
                      <td>{this.state.profit}</td>
                    </tr>
                    <tr>
                      <td>ROI</td>
                      <td>{this.state.roi}</td>
                    </tr>
                    <tr>
                      <td>Average</td>
                      <td>{this.state.avgProfit}</td>
                    </tr>
                  </MDBTableBody>
                </MDBTable>
              </MDBCardBody>
            </MDBCard>
          </MDBModalBody>
        </MDBModal>
      </MDBContainer>
    );
  }
}

export default Reconcile;
